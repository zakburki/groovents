Template.receipt.helpers({
  receipts: function(){
    Meteor.subscribe('receipts', Meteor.userId());
    return Receipts.find();
  }
});

Template.receipt.events({
  'click .item': function(evt){
    var id = $(evt.target).attr('id');
    Router.go('receiptDetail', {id:id});
  }
});


/*Template.receipt.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};*/