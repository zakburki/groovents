Template.events.helpers({
  events: function(){
    return Events.find();
  },
  count: function(id){
    var evt = Events.findOne({_id:id});
    var total = evt.attendees.length;
    var max = evt.people.total;
    return (max-total);
  }
});

Template.index.events({
  'click .splash': function(){
    Router.go('newEvent');
  }
});

Template.events.created = function () {
  this.autorun(function () {
    this.subscription = Meteor.subscribe('events');
  }.bind(this));
};

Template.events.rendered = function () {
  Session.set('ionTab.current','/events');
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};