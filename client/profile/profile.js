Template.profile.events({
  'submit form': function(e){

    e.preventDefault();

    var userId = Meteor.user()._id;

    var ccNum = $('#enteredCardNo').val();

    Meteor.call('saveCC', userId, ccNum, function (error, result) {
      if (error) {
        console.log(error);
      } else {
        console.log(result);
      }
    });
  }
});


Template.profile.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};